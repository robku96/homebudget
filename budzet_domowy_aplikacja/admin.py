from django.contrib import admin
from .models import Przychod, Wydatek, Skarbonka, Zestawienie

admin.site.register(Przychod)
admin.site.register(Wydatek)
admin.site.register(Skarbonka)
admin.site.register(Zestawienie)
