# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budzet_domowy_aplikacja', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='przychód',
            name='autor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='wydatek',
            name='autor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='przychód',
            name='data',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='wydatek',
            name='data',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
