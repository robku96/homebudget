# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budzet_domowy_aplikacja', '0002_auto_20170409_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='Przychod',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('kategoria', models.CharField(max_length=100)),
                ('typ', models.CharField(max_length=50)),
                ('nazwa', models.CharField(max_length=200)),
                ('kwota', models.FloatField()),
                ('data', models.DateField(default=django.utils.timezone.now)),
                ('autor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='przychód',
            name='autor',
        ),
        migrations.DeleteModel(
            name='Przychód',
        ),
    ]
