# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budzet_domowy_aplikacja', '0006_monthlyweatherbycity'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zestawienie',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('rok', models.CharField(max_length=5)),
                ('miesiac', models.CharField(max_length=30)),
                ('przychod', models.FloatField()),
                ('wydatek', models.FloatField()),
                ('roznica', models.FloatField()),
                ('autor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='MonthlyWeatherByCity',
        ),
    ]
