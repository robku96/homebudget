# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Przychód',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('kategoria', models.CharField(max_length=100)),
                ('typ', models.CharField(max_length=50)),
                ('nazwa', models.CharField(max_length=200)),
                ('kwota', models.FloatField()),
                ('data', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Wydatek',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('kategoria', models.CharField(max_length=100)),
                ('typ', models.CharField(max_length=50)),
                ('nazwa', models.CharField(max_length=200)),
                ('kwota', models.FloatField()),
                ('data', models.DateField()),
            ],
        ),
    ]
