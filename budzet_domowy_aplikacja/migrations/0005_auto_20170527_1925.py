# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budzet_domowy_aplikacja', '0004_auto_20170521_1945'),
    ]

    operations = [
        migrations.CreateModel(
            name='Skarbonka',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('stan', models.FloatField()),
                ('autor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='przychod',
            name='data',
            field=models.DateField(default=datetime.datetime(2017, 5, 27, 17, 25, 55, 509329, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='wydatek',
            name='data',
            field=models.DateField(),
        ),
    ]
