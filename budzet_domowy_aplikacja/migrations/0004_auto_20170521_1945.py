# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budzet_domowy_aplikacja', '0003_auto_20170409_2134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='przychod',
            name='data',
            field=models.DateField(blank=True, null=True),
        ),
    ]
