from django import forms


class PrzychodForm(forms.Form):
    kategoria = forms.CharField(label='kategoria')
    typ = forms.CharField(label='typ')
    nazwa = forms.CharField(label='nazwa', max_length=30)
    kwota = forms.FloatField(label='kwota',min_value=0)
    data = forms.DateField(label='data')

class WydatekForm(forms.Form):
    kategoria = forms.CharField(label='kategoria')
    typ = forms.CharField(label='typ')
    nazwa = forms.CharField(label='nazwa', max_length=200)
    kwota = forms.FloatField(label='kwota', min_value=0)
    data = forms.DateField(label='data')

class SkarbonkaForm(forms.Form):
    kwota = forms.FloatField(label='kwota', min_value=0)
    data = forms.DateField(label='data')
