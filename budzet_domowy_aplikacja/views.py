# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db.models import Sum
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.context_processors import csrf

from .forms import PrzychodForm, SkarbonkaForm
from .models import Przychod, Skarbonka, Zestawienie
from .models import Wydatek

def index(request):
    return render(request, 'budzet_domowy_aplikacja/index.html', {})

def podsumowanie_miesiaca(request):
    return render(request, 'budzet_domowy_aplikacja/podsumowanie_miesiaca_bez_wykresow.html')

def twoje_przychody(request):
    return render(request, 'budzet_domowy_aplikacja/twoje_przychody.html',{})

def skarbonka(request):
    autor = request.user
    if Skarbonka.objects.filter(autor=autor).exists() :
        ex = Skarbonka.objects.get(autor=autor)
        state = ex.stan
        return render(request,'budzet_domowy_aplikacja/skarbonka.html',{'state': state})
    else :
        state = 0.0
        return render(request,'budzet_domowy_aplikacja/skarbonka.html',{'state': state})

def planowane_wydatki(request):
    return render(request, 'budzet_domowy_aplikacja/planowane_wydatki.html', {})

def twoje_wydatki(request):
    return render(request, 'budzet_domowy_aplikacja/twoje_wydatki.html', {})

def bilans_roczny(request):
    return render(request, 'budzet_domowy_aplikacja/bilans_roczny.html')

def error(request):
    return render(request, 'budzet_domowy_aplikacja/added_income.html', {})

def log_in(request):
    c = {}
    c.update(csrf(request))
    username = request.POST.get('name', False)
    password = request.POST.get('password', False)
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return render(request, 'budzet_domowy_aplikacja/home.html', c)
    else:
        return render(request, 'budzet_domowy_aplikacja/index.html', c)

def log_out(request):
    logout(request)
    return redirect('index')

def register(request):
    c = {}
    c.update(csrf(request))
    username = request.POST['name_register']
    password = request.POST['password_register']
    email = request.POST['email']
    user = User.objects.create_user(username, email, password)
    if user.save() :
        return render(request, 'budzet_domowy_aplikacja/index.html', c)
    else :
        return render(request, 'budzet_domowy_aplikacja/index.html', c)

def add_income(request):
    if request.method == 'POST':
        form = PrzychodForm(request.POST)
        if form.is_valid():
             autor = request.user
             kategoria = request.POST.get('kategoria',False)
             typ = request.POST.get('typ',False)
             nazwa = request.POST.get('nazwa',False)
             kwota = request.POST.get('kwota',False)
             data = request.POST.get('data',False)
             income = Przychod(autor=autor,kategoria=kategoria,typ=typ,nazwa=nazwa,kwota=kwota,data=data)
             income.save()
             return render(request, 'budzet_domowy_aplikacja/added_income.html')
        else :
             return render(request, 'budzet_domowy_aplikacja/error_income.html')


def multiple_actions_on_incomes(request) :
    if request.method=='POST' and 'show' in request.POST:
        autor = request.user
        miesiac = request.POST.get('miesiac')
        year = int(request.POST.get('rok'))
        if  len(miesiac)<3 :
            query_result_1 = Przychod.objects.filter(autor=autor,data__year=year,data__month=miesiac,typ='Jednorazowy')
            query_result_2 = Przychod.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
            return render(request, 'budzet_domowy_aplikacja/twoje_przychody.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})
        else :
            if miesiac=="Styczeń":      x=1
            if miesiac=="Luty":         x=2
            if miesiac=="Marzec" :      x=3
            if miesiac=="Kwiecień" :    x=4
            if miesiac=="Maj" :         x=5
            if miesiac=="Czerwiec" :    x=6
            if miesiac=="Lipiec" :      x=7
            if miesiac=="Sierpień" :    x=8
            if miesiac=="Wrzesień" :    x=9
            if miesiac=="Październik" : x=10
            if miesiac=="Listopad" :    x=11
            if miesiac=="Grudzień" :    x=12
            query_result_1 = Przychod.objects.filter(autor=autor,data__year=year,data__month=x,typ='Jednorazowy')
            query_result_2 = Przychod.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
            return render(request, 'budzet_domowy_aplikacja/twoje_przychody.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})

    if request.method=='POST' and 'delete_non-cyclic' in request.POST:
        autor = request.user
        item_id = request.POST.get('delete_non-cyclic')
        month = request.POST.get('item_month_non-cyclic')
        year = request.POST.get('item_year_non-cyclic')
        item = Przychod.objects.get(id=item_id)
        item.delete()
        query_result_1 = Przychod.objects.filter(autor=autor,data__year=year,data__month=month,typ='Jednorazowy')
        query_result_2 = Przychod.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
        return render(request, 'budzet_domowy_aplikacja/twoje_przychody.html', {'query_result_1': query_result_1,'query_result_2': query_result_2 })

    if request.method=='POST' and 'delete_cyclic' in request.POST:
        autor = request.user
        item_id = request.POST.get('delete_cyclic')
        month = request.POST.get('item_month_cyclic')
        year = request.POST.get('item_year_cyclic')
        item = Przychod.objects.get(id=item_id)
        item.delete()
        query_result_1 = Przychod.objects.filter(autor=autor,data__year=year,data__month=month,typ='Jednorazowy')
        query_result_2 = Przychod.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
        return render(request, 'budzet_domowy_aplikacja/twoje_przychody.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})


def add_expense(request):
    if request.method == 'POST':
        form = PrzychodForm(request.POST)
        if form.is_valid():
             autor = request.user
             kategoria = request.POST.get('kategoria',False)
             typ = request.POST.get('typ',False)
             nazwa = request.POST.get('nazwa',False)
             kwota = request.POST.get('kwota',False)
             data = request.POST.get('data',False)
             expense = Wydatek(autor=autor,kategoria=kategoria,typ=typ,nazwa=nazwa,kwota=kwota,data=data)
             expense.save()
             return render(request, 'budzet_domowy_aplikacja/added_expense.html')
        else :
             return render(request, 'budzet_domowy_aplikacja/error_expense.html')

def multiple_actions_on_expenses(request) :
    if request.method=='POST' and 'show' in request.POST:
        autor = request.user
        miesiac = request.POST.get('miesiac')
        year = int(request.POST.get('rok'))
        if  len(miesiac)<3 :
            query_result_1 = Wydatek.objects.filter(autor=autor,data__year=year,data__month=miesiac,typ='Jednorazowy')
            query_result_2 = Wydatek.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
            return render(request, 'budzet_domowy_aplikacja/twoje_wydatki.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})
        else :
            if miesiac=="Styczeń":      x=1
            if miesiac=="Luty":         x=2
            if miesiac=="Marzec" :      x=3
            if miesiac=="Kwiecień" :    x=4
            if miesiac=="Maj" :         x=5
            if miesiac=="Czerwiec" :    x=6
            if miesiac=="Lipiec" :      x=7
            if miesiac=="Sierpień" :    x=8
            if miesiac=="Wrzesień" :    x=9
            if miesiac=="Październik" : x=10
            if miesiac=="Listopad" :    x=11
            if miesiac=="Grudzień" :    x=12
            query_result_1 = Wydatek.objects.filter(autor=autor,data__year=year,data__month=x,typ='Jednorazowy')
            query_result_2 = Wydatek.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
            return render(request, 'budzet_domowy_aplikacja/twoje_wydatki.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})

    if request.method=='POST' and 'delete_non-cyclic' in request.POST:
        autor = request.user
        item_id = request.POST.get('delete_non-cyclic')
        month = request.POST.get('item_month_non-cyclic')
        year = request.POST.get('item_year_non-cyclic')
        item = Wydatek.objects.get(id=item_id)
        item.delete()
        query_result_1 = Wydatek.objects.filter(autor=autor,data__year=year,data__month=month,typ='Jednorazowy')
        query_result_2 = Wydatek.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
        return render(request, 'budzet_domowy_aplikacja/twoje_wydatki.html', {'query_result_1': query_result_1,'query_result_2': query_result_2 })

    if request.method=='POST' and 'delete_cyclic' in request.POST:
        autor = request.user
        item_id = request.POST.get('delete_cyclic')
        month = request.POST.get('item_month_cyclic')
        year = request.POST.get('item_year_cyclic')
        item = Wydatek.objects.get(id=item_id)
        item.delete()
        query_result_1 = Wydatek.objects.filter(autor=autor,data__year=year,data__month=month,typ='Jednorazowy')
        query_result_2 = Wydatek.objects.filter(autor=autor,data__year=year,typ='Cykliczny')
        return render(request, 'budzet_domowy_aplikacja/twoje_wydatki.html', {'query_result_1': query_result_1,'query_result_2': query_result_2,})

def multiple_actions_on_piggy(request) :
    if request.method=='POST' and 'add_to_piggy' in request.POST:
        form = SkarbonkaForm(request.POST)
        if form.is_valid():
             autor = request.user
             kategoria = "Skarbonka"
             typ = "Jednorazowy"
             nazwa = "Wpłata do skarbonki"
             kwota = float(request.POST.get('kwota',False))
             data = request.POST.get('data',False)
             expense = Wydatek(autor=autor,kategoria=kategoria,typ=typ,nazwa=nazwa,kwota=kwota,data=data)
             expense.save()
             if Skarbonka.objects.filter(autor=autor).exists() :
                ex = Skarbonka.objects.get(autor=autor)
                ex.stan+=kwota
                ex.save()
                state = ex.stan
                return render(request, 'budzet_domowy_aplikacja/skarbonka.html',{'state': state})
             else :
                ex = Skarbonka(autor=autor, stan = 0)
                ex.stan+=kwota
                ex.save()
                state = ex.stan
                return render(request, 'budzet_domowy_aplikacja/skarbonka.html',{'state': state})

    if request.method=='POST' and 'delete_from_piggy' in request.POST:
        form = SkarbonkaForm(request.POST)
        if form.is_valid():
            autor = request.user
            kwota = float(request.POST.get('kwota',False))
            ex = Skarbonka.objects.get(autor=autor)
            if ex.stan>kwota :
                ex.stan-=kwota
                ex.save()
                state = ex.stan
            else :
                state = ex.stan
            return render(request,'budzet_domowy_aplikacja/skarbonka.html',{'state':state})

def savings(request):
    autor = request.user
    ex = Skarbonka.objects.get(autor=autor)
    state = ex.stan
    kwota_deklarowana = int(request.POST.get('kwota_deklarowana'))
    okres_oszczedzania = int(request.POST.get('okres_oszczedzania'))
    roczna_stopa_procentowa = int(request.POST.get('stopa_procentowa'))
    miesieczna_stopa_procentowa = (roczna_stopa_procentowa/12)/10
    wplacono = []
    na_koncie =[]

    for i in range(kwota_deklarowana,okres_oszczedzania*12*kwota_deklarowana+kwota_deklarowana,kwota_deklarowana):
        wplacono.append(i)

    for i in wplacono :
        na_koncie.append(i+(i*miesieczna_stopa_procentowa))
    return render(request,'budzet_domowy_aplikacja/skarbonka.html',{'state': state, 'wplacono':wplacono,'na_koncie':na_koncie})

def monthly_bilance(request):
    autor = request.user
    miesiac = request.POST.get('miesiac')
    year = int(request.POST.get('rok'))
    if  len(miesiac)<3 :
        wydatki_jednorazowe = Wydatek.objects.filter(autor=autor,data__year=year, data__month=miesiac, typ="Jednorazowy").aggregate(wydatki_jednorazowe=Sum('kwota'))['wydatki_jednorazowe']
        wydatki_cykliczne = Wydatek.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(wydatki_cykliczne=Sum('kwota'))['wydatki_cykliczne']
        if wydatki_jednorazowe is None :
            wydatki_jednorazowe = 0.0
        if wydatki_cykliczne is None :
            wydatki_cykliczne = 0.0
        wydatki = float(wydatki_jednorazowe + wydatki_cykliczne)

        przychody_jednorazowe = Przychod.objects.filter(autor=autor,data__year=year, data__month=miesiac, typ="Jednorazowy").aggregate(przychody_jednorazowe=Sum('kwota'))['przychody_jednorazowe']
        przychody_cykliczne = Przychod.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(przychody_cykliczne=Sum('kwota'))['przychody_cykliczne']
        if przychody_jednorazowe is None :
            przychody_jednorazowe = 0.0
        if przychody_cykliczne is None :
            przychody_cykliczne = 0.0
        przychody = float(przychody_jednorazowe + przychody_cykliczne)

        jedzenie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Jedzenie", typ="Jednorazowy").aggregate(jedzenie_jednorazowe=Sum('kwota'))['jedzenie_jednorazowe']
        jedzenie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Jedzenie", typ="Cykliczny").aggregate(jedzenie_cykliczne=Sum('kwota'))['jedzenie_cykliczne']
        if jedzenie_jednorazowe is None :
            jedzenie_jednorazowe = 0.0
        if jedzenie_cykliczne is None :
            jedzenie_cykliczne = 0.0
        jedzenie = float(jedzenie_jednorazowe + jedzenie_cykliczne)

        mieszkanie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Mieszkanie", typ="Jednorazowy").aggregate(mieszkanie_jednorazowe=Sum('kwota'))['mieszkanie_jednorazowe']
        mieszkanie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Mieszkanie", typ="Cykliczny").aggregate(mieszkanie_cykliczne=Sum('kwota'))['mieszkanie_cykliczne']
        if mieszkanie_jednorazowe is None :
            mieszkanie_jednorazowe = 0
        if mieszkanie_cykliczne is None :
            mieszkanie_cykliczne = 0
        mieszkanie = float(mieszkanie_jednorazowe + mieszkanie_cykliczne)

        kredyty_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Kredyty", typ="Jednorazowy").aggregate(kredyty_jednorazowe=Sum('kwota'))['kredyty_jednorazowe']
        kredyty_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Kredyty", typ="Cykliczny").aggregate(kredyty_cykliczne=Sum('kwota'))['kredyty_cykliczne']
        if kredyty_jednorazowe is None :
            kredyty_jednorazowe = 0.0
        if kredyty_cykliczne is None :
            kredyty_cykliczne = 0.0
        kredyty = float(kredyty_jednorazowe + kredyty_cykliczne)

        nauka_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Nauka", typ="Jednorazowy").aggregate(nauka_jednorazowe=Sum('kwota'))['nauka_jednorazowe']
        nauka_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Nauka", typ="Cykliczny").aggregate(nauka_cykliczne=Sum('kwota'))['nauka_cykliczne']
        if nauka_jednorazowe is None :
            nauka_jednorazowe = 0.0
        if nauka_cykliczne is None :
            nauka_cykliczne = 0.0
        nauka = float(nauka_jednorazowe + nauka_cykliczne)

        prezenty_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Prezenty", typ="Jednorazowy").aggregate(prezenty_jednorazowe=Sum('kwota'))['prezenty_jednorazowe']
        prezenty_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Prezenty", typ="Cykliczny").aggregate(prezenty_cykliczne=Sum('kwota'))['prezenty_cykliczne']
        if prezenty_jednorazowe is None :
            prezenty_jednorazowe = 0.0
        if prezenty_cykliczne is None :
            prezenty_cykliczne = 0.0
        prezenty = float(prezenty_jednorazowe + prezenty_cykliczne)

        ubezpieczenia_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Ubezpieczenia", typ="Jednorazowy").aggregate(ubezpieczenia_jednorazowe=Sum('kwota'))['ubezpieczenia_jednorazowe']
        ubezpieczenia_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Ubezpieczenia", typ="Cykliczny").aggregate(ubezpieczenia_cykliczne=Sum('kwota'))['ubezpieczenia_cykliczne']
        if ubezpieczenia_jednorazowe is None :
            ubezpieczenia_jednorazowe = 0.0
        if ubezpieczenia_cykliczne is None :
            ubezpieczenia_cykliczne = 0.0
        ubezpieczenia = float(ubezpieczenia_jednorazowe + ubezpieczenia_cykliczne)/2

        rozrywka_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Rozrywka", typ="Jednorazowy").aggregate(rozrywka_jednorazowe=Sum('kwota'))['rozrywka_jednorazowe']
        rozrywka_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Rozrywka", typ="Cykliczny").aggregate(rozrywka_cykliczne=Sum('kwota'))['rozrywka_cykliczne']
        if rozrywka_jednorazowe is None :
            rozrywka_jednorazowe = 0.0
        if rozrywka_cykliczne is None :
            rozrywka_cykliczne = 0.0
        rozrywka = float(rozrywka_jednorazowe + rozrywka_cykliczne)

        telekomunikacja_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Telekomunikacja", typ="Jednorazowy").aggregate(telekomunikacja_jednorazowe=Sum('kwota'))['telekomunikacja_jednorazowe']
        telekomunikacja_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Telekomunikacja", typ="Cykliczny").aggregate(telekomunikacja_cykliczne=Sum('kwota'))['telekomunikacja_cykliczne']
        if telekomunikacja_jednorazowe is None :
            telekomunikacja_jednorazowe = 0.0
        if telekomunikacja_cykliczne is None :
            telekomunikacja_cykliczne = 0.0
        telekomunikacja = float(telekomunikacja_jednorazowe + telekomunikacja_cykliczne)

        ubrania_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Ubrania", typ="Jednorazowy").aggregate(ubrania_jednorazowe=Sum('kwota'))['ubrania_jednorazowe']
        ubrania_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Ubrania", typ="Cykliczny").aggregate(ubrania_cykliczne=Sum('kwota'))['ubrania_cykliczne']
        if ubrania_jednorazowe is None :
            ubrania_jednorazowe = 0.0
        if ubrania_cykliczne is None :
            ubrania_cykliczne = 0.0
        ubrania = float(ubrania_jednorazowe + ubrania_cykliczne)

        higiena_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Higiena", typ="Jednorazowy").aggregate(higiena_jednorazowe=Sum('kwota'))['higiena_jednorazowe']
        higiena_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Higiena", typ="Cykliczny").aggregate(higiena_cykliczne=Sum('kwota'))['higiena_cykliczne']
        if higiena_jednorazowe is None :
            higiena_jednorazowe = 0.0
        if higiena_cykliczne is None :
            higiena_cykliczne = 0.0
        higiena = float(higiena_jednorazowe + higiena_cykliczne)

        transport_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Transport", typ="Jednorazowy").aggregate(transport_jednorazowe=Sum('kwota'))['transport_jednorazowe']
        transport_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Transport", typ="Cykliczny").aggregate(transport_cykliczne=Sum('kwota'))['transport_cykliczne']
        if transport_jednorazowe is None :
            transport_jednorazowe = 0.0
        if transport_cykliczne is None :
            transport_cykliczne = 0.0
        transport = float(transport_jednorazowe + transport_cykliczne)

        zdrowie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Zdrowie", typ="Jednorazowy").aggregate(zdrowie_jednorazowe=Sum('kwota'))['zdrowie_jednorazowe']
        zdrowie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Zdrowie", typ="Cykliczny").aggregate(zdrowie_cykliczne=Sum('kwota'))['zdrowie_cykliczne']
        if zdrowie_jednorazowe is None :
            zdrowie_jednorazowe = 0.0
        if zdrowie_cykliczne is None :
            zdrowie_cykliczne = 0.0
        zdrowie = float(zdrowie_jednorazowe + zdrowie_cykliczne)

        inne_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=miesiac, kategoria="Inne", typ="Jednorazowy").aggregate(inne_jednorazowe=Sum('kwota'))['inne_jednorazowe']
        inne_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Inne", typ="Cykliczny").aggregate(inne_cykliczne=Sum('kwota'))['inne_cykliczne']
        if inne_jednorazowe is None :
            inne_jednorazowe = 0.0
        if inne_cykliczne is None :
            inne_cykliczne = 0.0
        inne = float(inne_jednorazowe + inne_cykliczne)

        return render(request, 'budzet_domowy_aplikacja/podsumowanie_miesiaca.html', {'wydatki': wydatki,'przychody': przychody,'jedzenie': jedzenie,'mieszkanie': mieszkanie,
                                                                                      'kredyty': kredyty,'nauka': nauka,'prezenty': prezenty,'ubezpieczenia': ubezpieczenia,
                                                                                      'rozrywka': rozrywka,'telekomunikacja': telekomunikacja,'ubrania': ubrania,'higiena': higiena,
                                                                                      'transport': transport,'zdrowie': zdrowie,'inne': inne})
    else :
        if miesiac=="Styczeń":      x=1
        if miesiac=="Luty":         x=2
        if miesiac=="Marzec" :      x=3
        if miesiac=="Kwiecień" :    x=4
        if miesiac=="Maj" :         x=5
        if miesiac=="Czerwiec" :    x=6
        if miesiac=="Lipiec" :      x=7
        if miesiac=="Sierpień" :    x=8
        if miesiac=="Wrzesień" :    x=9
        if miesiac=="Październik" : x=10
        if miesiac=="Listopad" :    x=11
        if miesiac=="Grudzień" :    x=12
        wydatki_jednorazowe = Wydatek.objects.filter(autor=autor,data__year=year, data__month=x, typ="Jednorazowy").aggregate(wydatki_jednorazowe=Sum('kwota'))['wydatki_jednorazowe']
        wydatki_cykliczne = Wydatek.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(wydatki_cykliczne=Sum('kwota'))['wydatki_cykliczne']
        if wydatki_jednorazowe is None :
            wydatki_jednorazowe = 0.0
        if wydatki_cykliczne is None :
            wydatki_cykliczne = 0.0
        wydatki = float(wydatki_jednorazowe + wydatki_cykliczne)

        przychody_jednorazowe = Przychod.objects.filter(autor=autor,data__year=year, data__month=x, typ="Jednorazowy").aggregate(przychody_jednorazowe=Sum('kwota'))['przychody_jednorazowe']
        przychody_cykliczne = Przychod.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(przychody_cykliczne=Sum('kwota'))['przychody_cykliczne']
        if przychody_jednorazowe is None :
            przychody_jednorazowe = 0.0
        if przychody_cykliczne is None :
            przychody_cykliczne = 0.0
        przychody = float(przychody_jednorazowe + przychody_cykliczne)

        jedzenie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Jedzenie", typ="Jednorazowy").aggregate(jedzenie_jednorazowe=Sum('kwota'))['jedzenie_jednorazowe']
        jedzenie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Jedzenie", typ="Cykliczny").aggregate(jedzenie_cykliczne=Sum('kwota'))['jedzenie_cykliczne']
        if jedzenie_jednorazowe is None :
            jedzenie_jednorazowe = 0.0
        if jedzenie_cykliczne is None :
            jedzenie_cykliczne = 0.0
        jedzenie = float(jedzenie_jednorazowe + jedzenie_cykliczne)

        mieszkanie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Mieszkanie", typ="Jednorazowy").aggregate(mieszkanie_jednorazowe=Sum('kwota'))['mieszkanie_jednorazowe']
        mieszkanie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Mieszkanie", typ="Cykliczny").aggregate(mieszkanie_cykliczne=Sum('kwota'))['mieszkanie_cykliczne']
        if mieszkanie_jednorazowe is None :
            mieszkanie_jednorazowe = 0.0
        if mieszkanie_cykliczne is None :
            mieszkanie_cykliczne = 0.0
        mieszkanie = float(mieszkanie_jednorazowe + mieszkanie_cykliczne)

        kredyty_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Kredyty", typ="Jednorazowy").aggregate(kredyty_jednorazowe=Sum('kwota'))['kredyty_jednorazowe']
        kredyty_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Kredyty", typ="Cykliczny").aggregate(kredyty_cykliczne=Sum('kwota'))['kredyty_cykliczne']
        if kredyty_jednorazowe is None :
            kredyty_jednorazowe = 0.0
        if kredyty_cykliczne is None :
            kredyty_cykliczne = 0.0
        kredyty = float(kredyty_jednorazowe + kredyty_cykliczne)

        nauka_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Nauka", typ="Jednorazowy").aggregate(nauka_jednorazowe=Sum('kwota'))['nauka_jednorazowe']
        nauka_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Nauka", typ="Cykliczny").aggregate(nauka_cykliczne=Sum('kwota'))['nauka_cykliczne']
        if nauka_jednorazowe is None :
            nauka_jednorazowe = 0.0
        if nauka_cykliczne is None :
            nauka_cykliczne = 0.0
        nauka = float(nauka_jednorazowe + nauka_cykliczne)

        prezenty_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Prezenty", typ="Jednorazowy").aggregate(prezenty_jednorazowe=Sum('kwota'))['prezenty_jednorazowe']
        prezenty_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Prezenty", typ="Cykliczny").aggregate(prezenty_cykliczne=Sum('kwota'))['prezenty_cykliczne']
        if prezenty_jednorazowe is None :
            prezenty_jednorazowe = 0.0
        if prezenty_cykliczne is None :
            prezenty_cykliczne = 0.0
        prezenty = float(prezenty_jednorazowe + prezenty_cykliczne)

        ubezpieczenia_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Ubezpieczenia", typ="Jednorazowy").aggregate(ubezpieczenia_jednorazowe=Sum('kwota'))['ubezpieczenia_jednorazowe']
        ubezpieczenia_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Ubezpieczenia", typ="Cykliczny").aggregate(ubezpieczenia_cykliczne=Sum('kwota'))['ubezpieczenia_cykliczne']
        if ubezpieczenia_jednorazowe is None :
            ubezpieczenia_jednorazowe = 0.0
        if ubezpieczenia_cykliczne is None :
            ubezpieczenia_cykliczne = 0.0
        ubezpieczenia = float(ubezpieczenia_jednorazowe + ubezpieczenia_cykliczne)

        rozrywka_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Rozrywka", typ="Jednorazowy").aggregate(rozrywka_jednorazowe=Sum('kwota'))['rozrywka_jednorazowe']
        rozrywka_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Rozrywka", typ="Cykliczny").aggregate(rozrywka_cykliczne=Sum('kwota'))['rozrywka_cykliczne']
        if rozrywka_jednorazowe is None :
            rozrywka_jednorazowe = 0.0
        if rozrywka_cykliczne is None :
            rozrywka_cykliczne = 0.0
        rozrywka = float(rozrywka_jednorazowe + rozrywka_cykliczne)

        telekomunikacja_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Telekomunikacja", typ="Jednorazowy").aggregate(telekomunikacja_jednorazowe=Sum('kwota'))['telekomunikacja_jednorazowe']
        telekomunikacja_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Telekomunikacja", typ="Cykliczny").aggregate(telekomunikacja_cykliczne=Sum('kwota'))['telekomunikacja_cykliczne']
        if telekomunikacja_jednorazowe is None :
            telekomunikacja_jednorazowe = 0.0
        if telekomunikacja_cykliczne is None :
            telekomunikacja_cykliczne = 0.0
        telekomunikacja = float(telekomunikacja_jednorazowe + telekomunikacja_cykliczne)

        ubrania_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Ubrania", typ="Jednorazowy").aggregate(ubrania_jednorazowe=Sum('kwota'))['ubrania_jednorazowe']
        ubrania_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Ubrania", typ="Cykliczny").aggregate(ubrania_cykliczne=Sum('kwota'))['ubrania_cykliczne']
        if ubrania_jednorazowe is None :
            ubrania_jednorazowe = 0.0
        if ubrania_cykliczne is None :
            ubrania_cykliczne = 0.0
        ubrania = float(ubrania_jednorazowe + ubrania_cykliczne)

        higiena_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Higiena", typ="Jednorazowy").aggregate(higiena_jednorazowe=Sum('kwota'))['higiena_jednorazowe']
        higiena_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Higiena", typ="Cykliczny").aggregate(higiena_cykliczne=Sum('kwota'))['higiena_cykliczne']
        if higiena_jednorazowe is None :
            higiena_jednorazowe = 0.0
        if higiena_cykliczne is None :
            higiena_cykliczne = 0.0
        higiena = float(higiena_jednorazowe + higiena_cykliczne)

        transport_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Transport", typ="Jednorazowy").aggregate(transport_jednorazowe=Sum('kwota'))['transport_jednorazowe']
        transport_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Transport", typ="Cykliczny").aggregate(transport_cykliczne=Sum('kwota'))['transport_cykliczne']
        if transport_jednorazowe is None :
            transport_jednorazowe = 0.0
        if transport_cykliczne is None :
            transport_cykliczne = 0.0
        transport = float(transport_jednorazowe + transport_cykliczne)

        zdrowie_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Zdrowie", typ="Jednorazowy").aggregate(zdrowie_jednorazowe=Sum('kwota'))['zdrowie_jednorazowe']
        zdrowie_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Zdrowie", typ="Cykliczny").aggregate(zdrowie_cykliczne=Sum('kwota'))['zdrowie_cykliczne']
        if zdrowie_jednorazowe is None :
            zdrowie_jednorazowe = 0.0
        if zdrowie_cykliczne is None :
            zdrowie_cykliczne = 0.0
        zdrowie = float(zdrowie_jednorazowe + zdrowie_cykliczne)

        inne_jednorazowe = Wydatek.objects.filter(autor=autor, data__year=year, data__month=x, kategoria="Inne", typ="Jednorazowy").aggregate(inne_jednorazowe=Sum('kwota'))['inne_jednorazowe']
        inne_cykliczne = Wydatek.objects.filter(autor=autor, data__year=year, kategoria="Inne", typ="Cykliczny").aggregate(inne_cykliczne=Sum('kwota'))['inne_cykliczne']
        if inne_jednorazowe is None :
            inne_jednorazowe = 0.0
        if inne_cykliczne is None :
            inne_cykliczne = 0.0
        inne = float(inne_jednorazowe + inne_cykliczne)

        return render(request, 'budzet_domowy_aplikacja/podsumowanie_miesiaca.html', {'wydatki': wydatki,'przychody': przychody,'jedzenie': jedzenie,'mieszkanie': mieszkanie,
                                                                                      'kredyty': kredyty,'nauka':nauka,'prezenty':prezenty,'ubezpieczenia': ubezpieczenia,
                                                                                      'rozrywka': rozrywka,'telekomunikacja': telekomunikacja,'ubrania': ubrania,'higiena': higiena,
                                                                                      'transport': transport,'zdrowie': zdrowie,'inne':inne})

def bilance(request):
        autor = request.user
        year = int(request.POST.get('rok'))
        if Zestawienie.objects.filter(autor=autor,rok=year).exists() :
            for i in range(1,13):
                wydatki_jednorazowe = Wydatek.objects.filter(autor=autor,data__year=year, data__month=i, typ="Jednorazowy").aggregate(wydatki_jednorazowe=Sum('kwota'))['wydatki_jednorazowe']
                wydatki_cykliczne = Wydatek.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(wydatki_cykliczne=Sum('kwota'))['wydatki_cykliczne']
                if wydatki_jednorazowe is None :
                    wydatki_jednorazowe = 0.0
                if wydatki_cykliczne is None :
                    wydatki_cykliczne = 0.0
                wydatki = float(wydatki_jednorazowe + wydatki_cykliczne)
                przychody_jednorazowe = Przychod.objects.filter(autor=autor,data__year=year, data__month=i, typ="Jednorazowy").aggregate(przychody_jednorazowe=Sum('kwota'))['przychody_jednorazowe']
                przychody_cykliczne = Przychod.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(przychody_cykliczne=Sum('kwota'))['przychody_cykliczne']
                if przychody_jednorazowe is None :
                    przychody_jednorazowe = 0.0
                if przychody_cykliczne is None :
                    przychody_cykliczne = 0.0
                przychody = float(przychody_jednorazowe + przychody_cykliczne)
                if i==1 :  i = "Styczeń"
                if i==2 :  i = "Luty"
                if i==3 :  i = "Marzec"
                if i==4 :  i = "Kwiecień"
                if i==5 :  i = "Maj"
                if i==6 :  i = "Czerwiec"
                if i==7 :  i = "Lipiec"
                if i==8 :  i = "Sierpień"
                if i==9 :  i = "Wrzesień"
                if i==10 :  i = "Październik"
                if i==11 :  i = "Listopad"
                if i==12 :  i = "Grudzień"
                zestawienie = Zestawienie.objects.get(autor=autor,miesiac=i,rok=year)
                zestawienie.przychod = przychody
                zestawienie.wydatek = wydatki
                zestawienie.roznica = przychody - wydatki
                zestawienie.save()
            query = Zestawienie.objects.filter(autor=autor,rok=year)
            return render(request, 'budzet_domowy_aplikacja/bilans_roczny.html',{'query':query})
        else :
            for i in range(1,13) :
                wydatki_jednorazowe = Wydatek.objects.filter(autor=autor,data__year=year, data__month=i, typ="Jednorazowy").aggregate(wydatki_jednorazowe=Sum('kwota'))['wydatki_jednorazowe']
                wydatki_cykliczne = Wydatek.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(wydatki_cykliczne=Sum('kwota'))['wydatki_cykliczne']
                if wydatki_jednorazowe is None :
                    wydatki_jednorazowe = 0.0
                if wydatki_cykliczne is None :
                    wydatki_cykliczne = 0.0
                wydatki = float(wydatki_jednorazowe + wydatki_cykliczne)
                przychody_jednorazowe = Przychod.objects.filter(autor=autor,data__year=year, data__month=i, typ="Jednorazowy").aggregate(przychody_jednorazowe=Sum('kwota'))['przychody_jednorazowe']
                przychody_cykliczne = Przychod.objects.filter(autor=autor,data__year=year, typ="Cykliczny").aggregate(przychody_cykliczne=Sum('kwota'))['przychody_cykliczne']
                if przychody_jednorazowe is None :
                    przychody_jednorazowe = 0.0
                if przychody_cykliczne is None :
                    przychody_cykliczne = 0.0
                przychody = float(przychody_jednorazowe + przychody_cykliczne)
                if i==1 :  i = "Styczeń"
                if i==2 :  i = "Luty"
                if i==3 :  i = "Marzec"
                if i==4 :  i = "Kwiecień"
                if i==5 :  i = "Maj"
                if i==6 :  i = "Czerwiec"
                if i==7 :  i = "Lipiec"
                if i==8 :  i = "Sierpień"
                if i==9 :  i = "Wrzesień"
                if i==10 :  i = "Październik"
                if i==11 :  i = "Listopad"
                if i==12 :  i = "Grudzień"
                zestawienie = Zestawienie(autor=autor,rok=year,miesiac=i,przychod=przychody,wydatek=wydatki,roznica=przychody-wydatki)
                zestawienie.save()
            query = Zestawienie.objects.filter(autor=autor,rok=year)
            return render(request, 'budzet_domowy_aplikacja/bilans_roczny.html',{'query':query})

