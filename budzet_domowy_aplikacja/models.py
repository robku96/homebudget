from django.db import models

class Przychod(models.Model):
    autor = models.ForeignKey('auth.User')
    kategoria = models.CharField(max_length=100)
    typ = models.CharField(max_length=50)
    nazwa = models.CharField(max_length=200)
    kwota = models.FloatField()
    data = models.DateField()

class Wydatek(models.Model):
    autor = models.ForeignKey('auth.User')
    kategoria = models.CharField(max_length=100)
    typ = models.CharField(max_length=50)
    nazwa = models.CharField(max_length=200)
    kwota = models.FloatField()
    data = models.DateField()

class Skarbonka(models.Model):
    autor = models.ForeignKey('auth.User')
    stan = models.FloatField()

class Zestawienie(models.Model):
    autor = models.ForeignKey('auth.User')
    rok = models.CharField(max_length=5)
    miesiac = models.CharField(max_length=30)
    przychod = models.FloatField()
    wydatek = models.FloatField()
    roznica = models.FloatField()
