from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^index', views.index, name='index'),
    url(r'^home', views.log_in, name='home'),
    url(r'^podsumowanie_miesiaca', views.podsumowanie_miesiaca, name='podsumowanie_miesiaca'),
    url(r'^twoje_przychody', views.twoje_przychody, name='twoje_przychody'),
    url(r'^skarbonka', views.skarbonka, name='skarbonka'),
    url(r'^planowane_wydatki', views.planowane_wydatki, name='planowane_wydatki'),
    url(r'^twoje_wydatki', views.twoje_wydatki, name='twoje_wydatki'),
    url(r'^bilans_roczny', views.bilans_roczny, name='bilans_roczny'),
    url(r'^logout', views.log_out, name='logout'),
    url(r'^register', views.register, name='register'),
    url(r'^error', views.register, name='error'),
    url(r'^add_income', views.add_income, name='add_income'),
    url(r'^multiple_actions_on_incomes', views.multiple_actions_on_incomes, name='multiple_actions_on_incomes'),
    url(r'^add_expense', views.add_expense, name='add_expense'),
    url(r'^multiple_actions_on_expenses', views.multiple_actions_on_expenses, name='multiple_actions_on_expenses'),
    url(r'^multiple_actions_on_piggy', views.multiple_actions_on_piggy, name='multiple_actions_on_piggy'),
    url(r'^savings', views.savings, name='savings'),
    url(r'^monthly_bilance', views.monthly_bilance, name='monthly_bilance'),
    url(r'^bilance',views.bilance, name='bilance'),

]
